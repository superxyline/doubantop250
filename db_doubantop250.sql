/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3307
Source Database       : douban

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-08-08 16:50:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for db_doubantop250
-- ----------------------------
DROP TABLE IF EXISTS `db_doubantop250`;
CREATE TABLE `db_doubantop250` (
  `top` int(11) NOT NULL,
  `movie_name` varchar(255) DEFAULT NULL,
  `rank` varchar(255) DEFAULT NULL,
  `info` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `pic_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`top`)
) ENGINE=MyISAM AUTO_INCREMENT=1407 DEFAULT CHARSET=utf8;
